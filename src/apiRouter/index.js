import express from "express"
import swaggerDocsRouter from "../swaggerDocsRouter"
import { getImages, postImage } from "../controllers/images";

const apiRouter = express.Router();

apiRouter
    .get("/images", getImages)
    .post("/images", postImage)
    .use("/", swaggerDocsRouter);

export default apiRouter;
