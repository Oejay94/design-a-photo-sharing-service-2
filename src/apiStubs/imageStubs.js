export function getImagesResponse(){
    return {
        'imageURIs':[
            "https://codersera.com/blog/wp-content/uploads/2019/07/BLOG-23-L-3.jpg",
            "https://www.eff.org/files/banner_library/coder-cat-2.png" 
        ],
        'statusCode': 200
    }
}

export function postImageResponse(){
    return {
        'statusCode': 200
    }
}
