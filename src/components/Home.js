import React from "react";
import { getImages, postImage } from "../clientAPI/images";

class Home extends React.Component {
  state = { 
    getImages: { imageURIs: [], statusCode: 0 }, 
    postImage: { imageURI: "", statusCode: 0 },
    postImageError: "",
    postImageLoading: false
  };

  componentDidMount() {
    getImages().then(result => {
      this.setState({ ...this.state, getImages: result });
    });
  }

handleImageUpload = event => {
  event.preventDefault();
  const formData = new FormData(event.target);
  this.setState({ 
    ...this.state, 
    postImageError: "", 
  postImageLoading: true
 });
  postImage(formData).then(result => {
    this.setState({ 
      ...this.state, 
    postImage: result,
    postImageLoading: false
  });
  }).catch(err => {
    this.setState({ 
      ...this.state, 
      postImageError: err.message,
      postImageLoading: false
    });
  });
};

  render() {
    return (
    <>
    <h1>Kenziegram</h1>
    <form onSubmit={this.handleImageUpload}>
      <input type="file" />
      <button type="submit">Upload Image</button>
    </form>
    {this.state.postImageLoading && "Uploading..."}
    {this.state.postImage.statusCode === 200 && "Upload was successful!"}
    {this.state.postImageError && (
    <p style={{ color: "red" }}>{this.state.postImageError}</p>
      )}
    {this.state.getImages.imageURIs.map(uri => (
    <img key={uri} src={uri} />
    ))}
    </>
    );
  }
}

export default Home;
