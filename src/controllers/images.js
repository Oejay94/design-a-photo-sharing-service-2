import {getImagesResponse, postImageResponse} from "../apiStubs/imageStubs"

export const getImages = (request, response) => {
    console.log("getImages");
    response.send(getImagesResponse);
  };

  export const postImage = (request, response) => {
    console.log("postImage");
    response.send(postImageResponse);
  };
